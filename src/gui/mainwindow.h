/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:

    void debugMessage( QtMsgType msgType, const QString &msg );

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;

    class MainWindowPrivate;
    MainWindowPrivate *d;

private slots:

    void readDevice();
    void clearDevice();

    void matchFiles();

    void setupTools();
    void setupForms();

    void setInkFiles();
    void setScannedFiles();
    void setOutputDirectory();

    void renderInk();
    void preFilterForms();
    void extractRegions();
    void matchRegions();
    void optimizePairings();
    void translateInkToXML();
    void transformInk();
    void translateXMLToInk();
    void readInk();
    void finishProcessing();

    void beginProcessing();
    void endProcessing();

    void selectedInkFileChanged( const QString &inkFile );
    void showInkClicked();

    void showMatchingResults( const QString & = QString() );

    void selectedInkContentsEntryChanged( const QString &inkFile );
    void showInkContentsClicked();
};

#endif // MAINWINDOW_H
