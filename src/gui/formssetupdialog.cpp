/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "formssetupdialog.h"
#include "ui_formssetupdialog.h"

#include "formsconfiguration.h"

#include <QFileDialog>

FormsSetupDialog::FormsSetupDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormsSetupDialog)
{
    ui->setupUi(this);
    connect( this, SIGNAL(accepted()), this, SLOT(applySettings()) );
    connect( ui->addButton, SIGNAL(clicked()), this, SLOT(addFiles()) );
    connect( ui->removeButton, SIGNAL(clicked()), this, SLOT(removeFiles()) );
    connect( ui->clearButton, SIGNAL(clicked()), this, SLOT(clearFiles()) );

    ui->left->setValue( FormsConfiguration::instance()->formRect().left() );
    ui->top->setValue( FormsConfiguration::instance()->formRect().top() );
    ui->width->setValue( FormsConfiguration::instance()->formRect().width() );
    ui->height->setValue( FormsConfiguration::instance()->formRect().height() );
    foreach ( QString file, FormsConfiguration::instance()->forms() )
    {
        ui->forms->addItem( file );
    }
}

FormsSetupDialog::~FormsSetupDialog()
{
    delete ui;
}

void FormsSetupDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void FormsSetupDialog::applySettings()
{
    FormsConfiguration::instance()->setFormRect( QRect( ui->left->value(), ui->top->value(), ui->width->value(), ui->height->value() ) );
    QStringList forms;
    for ( int i = 0; i < ui->forms->count(); i++ )
    {
        forms << ui->forms->item( i )->text();
    }
    FormsConfiguration::instance()->setForms( forms );
}

void FormsSetupDialog::addFiles()
{
    QStringList files = QFileDialog::getOpenFileNames( this, tr( "Select Form Files" ), QString(), tr( "Form Definition Files (*.xml);;All (*.*)" ) );
    foreach ( QString file, files )
    {
        ui->forms->addItem( file );
    }
}

void FormsSetupDialog::removeFiles()
{
    for ( int i = ui->forms->count() - 1; i >= 0; i-- )
    {
        if ( ui->forms->isItemSelected( ui->forms->item( i ) ) )
        {
            delete ui->forms->takeItem( i );
        }
    }
}

void FormsSetupDialog::clearFiles()
{
    ui->forms->clear();
}
