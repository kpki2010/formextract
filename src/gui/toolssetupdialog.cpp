/*
    PROJECTNAME - PROJECTDESCRIPTION
    Copyright (C) 2010  USER <EMAIL>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "toolssetupdialog.h"
#include "ui_toolssetupdialog.h"

#include "toolsconfiguration.h"

#include <QFileDialog>

ToolsSetupDialog::ToolsSetupDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ToolsSetupDialog)
{
    ui->setupUi(this);
    connect( this, SIGNAL(accepted()), this, SLOT(applySettings()) );
    connect( ui->inkManagerButton, SIGNAL(clicked()), this, SLOT(setFile()) );
    connect( ui->imageFilterButton, SIGNAL(clicked()), this, SLOT(setFile()) );
    connect( ui->regionExtractorButton, SIGNAL(clicked()), this, SLOT(setFile()) );
    connect( ui->regionMatcherButton, SIGNAL(clicked()), this, SLOT(setFile()) );
    connect( ui->inkTransformerButton, SIGNAL(clicked()), this, SLOT(setFile()) );
    connect( ui->inkRecognizerButton, SIGNAL(clicked()), this, SLOT(setFile()) );
    ui->inkManager->setText( ToolsConfiguration::instance()->inkManager() );
    ui->imageFilter->setText( ToolsConfiguration::instance()->imageFilter() );
    ui->regionExtractor->setText( ToolsConfiguration::instance()->regionExtractor() );
    ui->regionMatcher->setText( ToolsConfiguration::instance()->regionMatcher() );
    ui->inkTransformer->setText( ToolsConfiguration::instance()->inkTransformer() );
    ui->inkRecognizer->setText( ToolsConfiguration::instance()->inkRecognizer() );
}

ToolsSetupDialog::~ToolsSetupDialog()
{
    delete ui;
}

void ToolsSetupDialog::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void ToolsSetupDialog::applySettings()
{
    ToolsConfiguration::instance()->setInkManager( ui->inkManager->text() );
    ToolsConfiguration::instance()->setImageFilter( ui->imageFilter->text() );
    ToolsConfiguration::instance()->setRegionExtractor( ui->regionExtractor->text() );
    ToolsConfiguration::instance()->setRegionMatcher( ui->regionMatcher->text() );
    ToolsConfiguration::instance()->setInkTransformer( ui->inkTransformer->text() );
    ToolsConfiguration::instance()->setInkRecognizer( ui->inkRecognizer->text() );
}

void ToolsSetupDialog::setFile()
{
    QWidget *button = qobject_cast< QWidget* >( sender() );
    QLineEdit *edit = 0;
    if ( button == ui->inkManagerButton )
    {
        edit = ui->inkManager;
    } else if ( button == ui->imageFilterButton )
    {
        edit = ui->imageFilter;
    } else if ( button == ui->regionExtractorButton )
    {
        edit = ui->regionExtractor;
    } else if ( button == ui->regionMatcherButton )
    {
        edit = ui->regionMatcher;
    } else if ( button == ui->inkTransformerButton )
    {
        edit = ui->inkTransformer;
    } else if ( button == ui->inkRecognizerButton )
    {
        edit = ui->inkRecognizer;
    }

    if ( edit )
    {
        QString file = QFileDialog::getOpenFileName( this, tr( "Select Executable" ), edit->text() );
        if ( !( file.isEmpty() ) )
        {
            edit->setText( file );
        }
    }
}
