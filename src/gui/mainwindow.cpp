/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "debugmessagehandler.h"
#include "formssetupdialog.h"
#include "imagefilter.h"
#include "inkmanager.h"
#include "inkrecognizer.h"
#include "inktransformer.h"
#include "regionextractor.h"
#include "regionmatcher.h"
#include "regionpairoptimizerthread.h"
#include "toolssetupdialog.h"

#include "formsconfiguration.h"

#include <QDebug>
#include <QFileDialog>
#include <QGraphicsScene>
#include <QMessageBox>


class MainWindow::MainWindowPrivate
{

public:

    typedef struct
    {
        QString inkFile;
        QString renderedInkFile;
        QString regionsFile;
        QString scannedFile;
        QString matchingFile;
        QString inkXML;
        QString transFormedInkXML;
        QString transformedInk;
        QString formFile;
        QString readContents;
    } InkChain;

    typedef struct
    {
        QString scannedFile;
        QString filteredFile;
        QString regionsFile;
        QString inkFile;
        QString matchingFile;
    } ScannedChain;

    typedef struct
    {
        QList< InkChain > inkChains;
        QList< ScannedChain > scannedChains;
        QList< QString > matchingFiles;
    } FileChains;

    QMenu *deviceMenu;
    QMenu *actionsMenu;
    QMenu *settingsMenu;

    QAction *readDeviceAction;
    QAction *clearDeviceAction;

    QAction *extractFormDataAction;

    QAction *setupToolsAction;
    QAction *setupFormsAction;

    QGraphicsScene *inkScene;
    QGraphicsScene *scannedScene;

    QStringList inkFiles;
    QStringList scannedFiles;

    QHash< RegionPair, QString > matchingResults;
    FileChains chains;

    InkManager inkManager;
    ImageFilter formPreFilter;
    RegionExtractor regionExtractor;
    RegionMatcher regionMatcher;
    RegionPairOptimizerThread optimizerThread;
    InkTransformer inkTransformer;
    InkRecognizer inkRecognizer;

    MainWindowPrivate( MainWindow *mw )
        : inkScene( new QGraphicsScene( mw ) ),
          scannedScene( new QGraphicsScene( mw ) ),
          mainWindow( mw )
    {
    }

    virtual ~MainWindowPrivate()
    {
    }

    void initialize()
    {
        createMenus();
        createActions();
        setupActions();
        setupMatchingChain();
    }

    void buildChains1()
    {
        qDebug() << "Building chains, part #1 ...";
        chains.inkChains.clear();
        chains.scannedChains.clear();
        foreach ( QString inkFile, inkFiles )
        {
            InkChain ic;
            ic.inkFile = inkFile;
            ic.renderedInkFile = inkManager.rasteredImages().value( inkFile );
            ic.regionsFile = regionExtractor.extractedRegions().value( ic.renderedInkFile );
            ic.scannedFile = QString();
            if ( FormsConfiguration::instance()->forms().length() > 0 )
            {
                //TODO: In a multi-form environment, the correct form definition file must be found out!
                ic.formFile = FormsConfiguration::instance()->forms().first();
            }
            chains.inkChains.append( ic );
            qDebug() << "InkChain: " << ic.inkFile << ic.renderedInkFile << ic.regionsFile;
        }

        foreach ( QString scannedFile, scannedFiles )
        {
            ScannedChain sc;
            sc.scannedFile = scannedFile;
            sc.filteredFile = formPreFilter.filteredImages().value( scannedFile );
            sc.regionsFile = regionExtractor.extractedRegions().value( sc.filteredFile );
            sc.inkFile = QString();
            chains.scannedChains.append( sc );
            qDebug() << "ScannedChain: " << sc.scannedFile << sc.filteredFile << sc.regionsFile;
        }

        chains.matchingFiles = regionMatcher.matchedFiles().values();

        foreach ( RegionPair pair, optimizerThread.pairs() )
        {
            qDebug() << "Pair: " << pair.first << pair.second;
            for ( int i = 0; i < chains.inkChains.length(); i++ )
            {
                if ( chains.inkChains[ i ].regionsFile == pair.first )
                {
                    qDebug() << "Part 1 found..";
                    for ( int j = 0; j < chains.scannedChains.length(); j++ )
                    {
                        if ( chains.scannedChains[ j ].regionsFile == pair.second )
                        {
                            qDebug() << "Part 2 found..";
                            chains.inkChains[ i ].scannedFile = chains.scannedChains[ j ].scannedFile;
                            chains.scannedChains[ j ].inkFile = chains.inkChains[ i ].inkFile;
                            chains.scannedChains[ j ].matchingFile = chains.inkChains[ i ].matchingFile =
                                                                     regionMatcher.matchedFiles().value( pair );
                            break;
                        }
                    }
                    break;
                }
            }
        }

    }

    void buildChains2()
    {
        qDebug() << "Building chains, part #2 ...";
        for ( int i = 0; i < chains.inkChains.length(); i++ )
        {
            chains.inkChains[ i ].inkXML = inkManager.inkXMLFiles().value( chains.inkChains[ i ].inkFile );
            chains.inkChains[ i ].transFormedInkXML = inkTransformer.transformedFiles().value(
                    InkPair( chains.inkChains[ i ].inkXML,
                             chains.inkChains[ i ].matchingFile ) );
            chains.inkChains[ i ].transformedInk = inkManager.inkFiles().value( chains.inkChains[ i ].transFormedInkXML );
        }
    }

    void buildChains3()
    {
        qDebug() << "Building chains, part #3 ...";
        for ( int i = 0; i < chains.inkChains.length(); i++ )
        {
            chains.inkChains[ i ].readContents = inkRecognizer.readFiles().value(
                    InkFormPair( chains.inkChains[ i ].transformedInk,
                                 chains.inkChains[ i ].formFile ) );
        }
    }

    InkChain inkChain( const QString &inkFile )
    {
        foreach ( InkChain ic, chains.inkChains )
        {
            if ( ic.inkFile == inkFile )
            {
                return ic;
            }
        }
        return InkChain();
    }

    ScannedChain scannedChain( const QString &scannedFile )
    {
        foreach ( ScannedChain sc, chains.scannedChains )
        {
            if ( sc.scannedFile == scannedFile )
            {
                return sc;
            }
        }
        return ScannedChain();
    }

private:

    MainWindow *mainWindow;

    void createMenus()
    {
        deviceMenu = mainWindow->menuBar()->addMenu( QObject::tr( "Device" ) );
        actionsMenu = mainWindow->menuBar()->addMenu( QObject::tr( "Actions" ) );
        settingsMenu = mainWindow->menuBar()->addMenu( QObject::tr( "Settings" ) );
    }

    void createActions()
    {
        readDeviceAction = deviceMenu->addAction( QObject::tr( "Read" ) );
        clearDeviceAction = deviceMenu->addAction( QObject::tr( "Clear" ) );

        extractFormDataAction = actionsMenu->addAction( QObject::tr( "Extract Form Data" ) );

        setupToolsAction = settingsMenu->addAction( QObject::tr( "Setup Tools..." ) );
        setupFormsAction = settingsMenu->addAction( QObject::tr( "Setup Forms..." ) );
    }

    void setupActions()
    {
        QObject::connect( readDeviceAction, SIGNAL(triggered()), mainWindow, SLOT(readDevice()) );
        QObject::connect( clearDeviceAction, SIGNAL(triggered()), mainWindow, SLOT(clearDevice()) );
        QObject::connect( extractFormDataAction, SIGNAL(triggered()), mainWindow, SLOT(matchFiles()) );
        QObject::connect( setupToolsAction, SIGNAL(triggered()), mainWindow, SLOT(setupTools()) );
        QObject::connect( setupFormsAction, SIGNAL(triggered()), mainWindow, SLOT(setupForms()) );
    }

    void setupMatchingChain()
    {
        QObject::connect( &inkManager,     SIGNAL(rasteredInk()),
                          mainWindow,       SLOT(preFilterForms()) );
        QObject::connect( &formPreFilter,   SIGNAL(filteredScannedImages()),
                          mainWindow,       SLOT(extractRegions()) );
        QObject::connect( &regionExtractor, SIGNAL(finishedExtractingRegions()),
                          mainWindow,       SLOT(matchRegions()) );
        QObject::connect( &regionMatcher,   SIGNAL(finishedMatchingRegions()),
                          mainWindow,       SLOT(optimizePairings()) );
        QObject::connect( &optimizerThread, SIGNAL(finished()),
                          mainWindow,       SLOT(translateInkToXML()) );
        QObject::connect( &inkManager,      SIGNAL(convertedToInkXML()),
                          mainWindow,       SLOT(transformInk()) );
        QObject::connect( &inkTransformer,  SIGNAL(finishedTransforming()),
                          mainWindow,       SLOT(translateXMLToInk()) );
        QObject::connect( &inkManager,      SIGNAL(convertedToInk()),
                          mainWindow,       SLOT(readInk()) );
        QObject::connect( &inkRecognizer,   SIGNAL(finishedReading()),
                          mainWindow,       SLOT(finishProcessing()) );
    }

};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    d( new MainWindowPrivate( this ) )
{
    ui->setupUi(this);
    d->initialize();

    statusBar()->showMessage( tr( "Ready" ) );

    ui->inkView->setScene( d->inkScene );
    ui->scannedView->setScene( d->scannedScene );


    DebugMessageHandler *dmh = new DebugMessageHandler( qApp );
    connect( dmh, SIGNAL(messageReceived(QtMsgType,QString)), this, SLOT(debugMessage(QtMsgType,QString)) );

    connect( ui->setInkFilesButton,         SIGNAL(clicked()),
             this,                          SLOT(setInkFiles()) );
    connect( ui->setScannedFilesButton,     SIGNAL(clicked()),
             this,                          SLOT(setScannedFiles()) );
    connect( ui->setOutputButton,           SIGNAL(clicked()),
             this,                          SLOT(setOutputDirectory()) );
    connect( ui->inkSelector,               SIGNAL(currentIndexChanged(QString)),
             this,                          SLOT(selectedInkFileChanged(QString)) );
    connect( ui->showButton,                SIGNAL(clicked()),
             this,                          SLOT(showInkClicked()) );
    connect( ui->inkMRSelector,             SIGNAL(currentIndexChanged(QString)),
             this,                          SLOT(showMatchingResults()) );
    connect( ui->scannedMRSelector,         SIGNAL(currentIndexChanged(QString)),
             this,                          SLOT(showMatchingResults()) );
    connect( ui->showMatchingResultsButton, SIGNAL(clicked()),
             this,                          SLOT(showMatchingResults()) );
    connect( ui->inkContentsSelector,       SIGNAL(currentIndexChanged(QString)),
             this,                          SLOT(selectedInkContentsEntryChanged(QString)) );
    connect( ui->inkContentsButton,         SIGNAL(clicked()),
             this,                          SLOT(showInkContentsClicked()) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::debugMessage( QtMsgType msgType, const QString &msg )
{
    QString color;
    switch ( msgType )
    {
    case QtDebugMsg:    color = "#000000"; break;
    case QtWarningMsg:  color = "#FFBB00"; break;
    case QtCriticalMsg: color = "#FF0000"; break;
    case QtFatalMsg:    color = "#FFFF00"; break;
    }
    ui->messages->append( QString( "<span style=\"color: %1;\">%2</span>" ).arg( color ).arg( msg ) );
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::readDevice()
{
    InkManager manager;
    QString dir = QFileDialog::getExistingDirectory( this, tr( "Select Target Directory" ) );
    manager.readDevice( dir );
    qDebug() << tr( "Start reading device..." );
    while ( manager.isReadingDevice() )
    {
        QApplication::processEvents();
    }
    qDebug() << tr( "Read device" );
}

void MainWindow::clearDevice()
{
    InkManager manager;
    qDebug() << tr( "Clearing device..." );
    manager.clearDevice();
    qDebug() << tr( "Done!" );
}

void MainWindow::matchFiles()
{
    if ( ui->inputTab->isEnabled() )
    {
        if ( d->inkFiles.length() == 0 )
        {
            QMessageBox::information( this, tr( "No Digital Ink" ),
                                      tr( "No Ink files selected. Please select some Ink files first!" ) );
            return;
        }
        if ( d->scannedFiles.length() == 0 )
        {
            QMessageBox::information( this, tr( "No Scanned Forms" ),
                                      tr( "No scanned forms selected. Please select some scanned images first!" ) );
            return;
        }
        QFileInfo outputDirectory( ui->outputDirectory->text() );
        if ( !( outputDirectory.isDir() ) )
        {
            QMessageBox::information( this, tr( "No Output Directory Set" ),
                                      tr( "No output directory selected yet. Please select one first!" ) );
            return;
        }
        qDebug() << "Start processing...";
        beginProcessing();
        renderInk();
    }
}

void MainWindow::setupTools()
{
    ToolsSetupDialog d;
    d.exec();
}

void MainWindow::setupForms()
{
    FormsSetupDialog d;
    d.exec();
}

void MainWindow::setInkFiles()
{
    if ( ui->inputTab->isEnabled() )
    {
        QStringList files = QFileDialog::getOpenFileNames( this, tr( "Load Ink Files" ),
                                                           QString(), tr( "Digital Ink (*.ink)" ) );
        if ( files.length() > 0 )
        {
            d->inkFiles = files;
            ui->inkFiles->clear();
            foreach ( QString file, files )
            {
                ui->inkFiles->addItem( file );
            }
        }
    }
}

void MainWindow::setScannedFiles()
{
    if ( ui->inputTab->isEnabled() )
    {
        QStringList files = QFileDialog::getOpenFileNames( this, tr( "Load Scanned Forms" ),
                                                           QString(), tr( "All Files (*.*)" ) );
        if ( files.length() > 0 )
        {
            d->scannedFiles = files;
            ui->scannedFiles->clear();
            foreach ( QString file, files )
            {
                ui->scannedFiles->addItem( file );
            }
        }
    }
}

void MainWindow::setOutputDirectory()
{
    if ( ui->inputTab->isEnabled() )
    {
        QString dir = QFileDialog::getExistingDirectory( this, tr( "Set Output Directory" ) );
        if ( !( dir.isEmpty() ) )
        {
            ui->outputDirectory->setText( dir );
        }
    }
}

void MainWindow::renderInk()
{
    statusBar()->showMessage( tr( "Rendering Ink Files..." ) );
    qDebug() << "Rendering Ink files...";
    d->inkManager.rasterInk( d->inkFiles, ui->outputDirectory->text() );
}

void MainWindow::preFilterForms()
{
    statusBar()->showMessage( tr( "Filtering Scanned Form Images..." ) );
    qDebug() << "Filtering scanned form images...";
    d->formPreFilter.filterScannedImages( d->scannedFiles, ui->outputDirectory->text() );
}

void MainWindow::extractRegions()
{
    statusBar()->showMessage( tr( "Extraction Regions..." ) );
    qDebug() << "Building list of region source files...";
    QStringList files;
    foreach ( QString file, d->inkManager.rasteredImages().values())
    {
        files << file;
    }
    foreach ( QString file, d->formPreFilter.filteredImages().values() )
    {
        files << file;
    }
    qDebug() << "Extracting regions...";
    d->regionExtractor.extractRegions( files, ui->outputDirectory->text() );
}

void MainWindow::matchRegions()
{
    statusBar()->showMessage( tr( "Matching Regions...") );
    qDebug() << "Building list of region files to compare...";
    QList< RegionPair > pairs;
    foreach ( QString inkFile, d->inkFiles )
    {
        QString rasteredInkFile = d->inkManager.rasteredImages().value( inkFile );
        QString rasteredInkRegions = d->regionExtractor.extractedRegions().value( rasteredInkFile );

        qDebug() << "Ink keys: " << d->inkManager.rasteredImages().keys();
        qDebug() << "Ink values: " << d->inkManager.rasteredImages().values();
        qDebug() << "Scanned keys: " << d->formPreFilter.filteredImages().keys();
        qDebug() << "Scanned values: " << d->formPreFilter.filteredImages().values();

        foreach( QString scannedFile, d->scannedFiles )
        {
            QString filteredScannedFile = d->formPreFilter.filteredImages().value( scannedFile );
            QString filteredScannedRegions = d->regionExtractor.extractedRegions().value( filteredScannedFile );

            qDebug() << "Ink:     " << rasteredInkFile << rasteredInkRegions;
            qDebug() << "Scanned: " << filteredScannedFile << filteredScannedRegions;

            RegionPair pair( rasteredInkRegions, filteredScannedRegions );
            pairs << pair;
        }
    }
    qDebug() << "Matching pairs...";
    d->regionMatcher.matchRegions( pairs, ui->outputDirectory->text() );
}

void MainWindow::optimizePairings()
{
    statusBar()->showMessage( tr( "Calculating Optimal Pairings..." ) );
    qDebug() << "Start finding optimal pairings...";
    d->optimizerThread.setMatchingResults( d->regionMatcher.matchedFiles() );
    d->optimizerThread.start();
}

void MainWindow::translateInkToXML()
{
    statusBar()->showMessage( tr( "Translating Ink to InkXML..." ) );
    qDebug() << "Building chains...";
    d->buildChains1();
    qDebug() << "Converting Ink to InkXML...";
    d->inkManager.convertToInkXML( d->inkFiles, ui->outputDirectory->text() );
}

void MainWindow::transformInk()
{
    statusBar()->showMessage( tr( "Applying transformations to ink..." ) );
    qDebug() << "Transforming InkXMLs...";
    QList< InkPair > inkPairs;
    foreach ( MainWindowPrivate::InkChain ic, d->chains.inkChains )
    {
        InkPair ip( d->inkManager.inkXMLFiles().value( ic.inkFile ), ic.matchingFile );
        inkPairs << ip;
    }
    d->inkTransformer.transform( inkPairs, ui->outputDirectory->text() );
}

void MainWindow::translateXMLToInk()
{
    statusBar()->showMessage( tr( "Translating InkXML to Ink..." ) );
    qDebug() << "Converting transformed InkXMLs to Ink...";
    d->inkManager.convertToInk( d->inkTransformer.transformedFiles().values(), ui->outputDirectory->text() );
}

void MainWindow::readInk()
{
    d->buildChains2();
    statusBar()->showMessage( tr( "Reading Ink contents..." ) );
    qDebug() << "Reading Ink contents...";
    QList< InkFormPair > inkFormPairs;
    foreach ( MainWindowPrivate::InkChain ic, d->chains.inkChains )
    {
        inkFormPairs << InkFormPair( ic.transformedInk, ic.formFile );
    }
    d->inkRecognizer.read( inkFormPairs, ui->outputDirectory->text() );
}

void MainWindow::finishProcessing()
{
    d->buildChains3();
    statusBar()->showMessage( tr( "Ready" ) );
    qDebug() << "Finished processing!";
    endProcessing();
}

void MainWindow::beginProcessing()
{
    ui->inputTab->setEnabled( false );
}

void MainWindow::endProcessing()
{
    d->matchingResults = d->regionMatcher.matchedFiles();
    ui->inkSelector->clear();
    ui->inkSelector->addItems( d->inkFiles );
    ui->inkMRSelector->clear();
    ui->inkMRSelector->addItems( d->inkFiles );
    ui->scannedMRSelector->clear();
    ui->scannedMRSelector->addItems( d->scannedFiles );
    ui->inkContentsSelector->clear();
    ui->inkContentsSelector->addItems( d->inkFiles );
    d->inkScene->clear();
    d->inkScene->setSceneRect( QRect() );
    d->scannedScene->clear();
    d->scannedScene->setSceneRect( QRect() );
    ui->inputTab->setEnabled( true );
}

void MainWindow::selectedInkFileChanged( const QString &inkFile )
{
    d->inkScene->clear();
    d->inkScene->setSceneRect( QRect( 0, 0, 0, 0 ) );
    foreach ( MainWindowPrivate::InkChain inkChain, d->chains.inkChains )
    {
        if ( inkChain.inkFile == inkFile )
        {
            QPixmap image( inkChain.renderedInkFile );
            d->inkScene->addPixmap( image );
            break;
        }
    }

    d->scannedScene->clear();
    d->scannedScene->setSceneRect( QRect( 0, 0, 0, 0 ) );
    foreach ( MainWindowPrivate::ScannedChain scannedChain, d->chains.scannedChains )
    {
        if ( scannedChain.inkFile == inkFile )
        {
            QPixmap image( scannedChain.scannedFile );
            d->scannedScene->addPixmap( image );
            break;
        }
    }
}

void MainWindow::showInkClicked()
{
    selectedInkFileChanged( ui->inkSelector->currentText() );
}

void MainWindow::showMatchingResults( const QString & )
{
    QString first = QString();
    QString second = QString();
    foreach ( MainWindowPrivate::InkChain ic, d->chains.inkChains )
    {
        if ( ic.inkFile == ui->inkMRSelector->currentText() )
        {
            first = ic.regionsFile;
            break;
        }
    }
    foreach ( MainWindowPrivate::ScannedChain sc, d->chains.scannedChains )
    {
        if ( sc.scannedFile == ui->scannedMRSelector->currentText() )
        {
            second = sc.regionsFile;
            break;
        }
    }
    RegionPair key( first, second );
    QString matchingFile = d->matchingResults.value( key, QString() );
    if ( !( matchingFile.isEmpty() ) )
    {
        QFile f( matchingFile );
        if ( f.open( QIODevice::ReadOnly ) )
        {
            ui->matchingResults->setPlainText( f.readAll() );
            f.close();
        } else
        {
            ui->matchingResults->clear();
        }
    }
}

void MainWindow::selectedInkContentsEntryChanged( const QString &inkFile )
{
    ui->inkContents->clear();
    MainWindowPrivate::InkChain ic = d->inkChain( inkFile );
    if ( !( ic.readContents.isEmpty() ) )
    {
        QFile f( ic.readContents );
        if ( f.open( QIODevice::ReadOnly ) )
        {
            ui->inkContents->append( f.readAll() );
            f.close();
        }
    }
}

void MainWindow::showInkContentsClicked()
{
    selectedInkContentsEntryChanged( ui->inkContentsSelector->currentText() );
}
