TEMPLATE = app
TARGET = formextract
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release
CONFIG(debug, debug|release):LIBS += -L../../bin/Debug
else:LIBS += -L../../bin/Release
LIBS += -lformextractutils
HEADERS = mainwindow.h \
    toolssetupdialog.h \
    formssetupdialog.h
SOURCES = mainwindow.cpp \
    main.cpp \
    toolssetupdialog.cpp \
    formssetupdialog.cpp
FORMS = mainwindow.ui \
    toolssetupdialog.ui \
    formssetupdialog.ui
INCLUDEPATH += ../lib
