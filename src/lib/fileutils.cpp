/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fileutils.h"

#include <QFileInfo>

const QString FileUtils::findNonExistingFileName( const QString &dir, const QString &base, const QString &ext )
{
    int i = 0;
    while ( true )
    {
        QString fileName = QString( "%1/%2%3%4%5" ).arg( dir ).arg( base ).arg( base.isEmpty() ? "" : "-" ).arg( i ).arg( ext );
        QFileInfo fi( fileName );
        if ( !( fi.exists() ) )
        {
            return fileName;
        }
        i++;
    }
}

bool FileUtils::fileExists( const QString &fileName )
{
    QFileInfo fi( fileName );
    return fi.exists();
}

const QString FileUtils::basename( const QString &filename )
{
    QFileInfo fi( filename );
    return fi.baseName();
}
