/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionmatcher.h"

#include "fileutils.h"
#include "programrunner.h"
#include "toolsconfiguration.h"

#include <QDebug>


uint qHash( const RegionPair &p )
{
    return qHash( p.first ) ^ qHash( p.second );
}


class RegionMatcher::RegionMatcherPrivate
{

public:

    QList< RegionPair >             pairs;
    QString                         targetDirectory;
    QHash< RegionPair, QString >    matchedFiles;
    int                             currentIndex;
    ProgramRunner                  *runner;



    RegionMatcherPrivate( RegionMatcher *rm )
        : pairs( QList< RegionPair >() ),
           targetDirectory( QString() ),
           matchedFiles( QHash< RegionPair, QString >() ),
           currentIndex( -1 ),
           runner( new ProgramRunner( rm ) ),
           regionMatcher( rm )
    {
    }

    virtual ~RegionMatcherPrivate()
    {
    }

private:

    RegionMatcher *regionMatcher;

};

RegionMatcher::RegionMatcher( QObject *parent )
    : QObject( parent ),
      d( new RegionMatcherPrivate( this ) )
{
    connect( d->runner, SIGNAL(finished(bool,bool)), this, SLOT(finishedMatchingRegionPair(bool,bool)) );
}

RegionMatcher::~RegionMatcher()
{
    delete d;
}

bool RegionMatcher::matchRegions( QList<RegionPair>regions, const QString &targetDirectory )
{
    if ( isMatchingRegions() || regions.length() == 0 )
    {
        return false;
    } else
    {
        d->pairs = regions;
        d->targetDirectory = targetDirectory;
        d->currentIndex = 0;
        d->matchedFiles.clear();
        return startMatchingCurrent();
    }
}

bool RegionMatcher::isMatchingRegions() const
{
    return d->currentIndex >= 0;
}

const QHash< RegionPair, QString > &RegionMatcher::matchedFiles() const
{
    return d->matchedFiles;
}

bool RegionMatcher::startMatchingCurrent()
{
    RegionPair input = d->pairs[ d->currentIndex ];
    QString output = FileUtils::findNonExistingFileName( d->targetDirectory, "matched", ".xml" );
    d->matchedFiles.insert( input, output );
    QStringList args;
    args << "--input" << input.first << input.second << "--output" << output;
    qDebug() << "Start matching regions pair...";
    qDebug() << "Arguments: " << args;
    return d->runner->run( ToolsConfiguration::instance()->regionMatcher(), args );
}

void RegionMatcher::finishedMatchingRegionPair( bool, bool )
{
    d->currentIndex++;
    if ( d->currentIndex < d->pairs.length() )
    {
        startMatchingCurrent();
    } else
    {
        d->currentIndex = -1;
        d->pairs.clear();
        d->targetDirectory = QString();
        emit finishedMatchingRegions();
    }
}
