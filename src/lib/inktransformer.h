/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INKTRANSFORMER_H
#define INKTRANSFORMER_H

#include "formextractutilsdefines.h"

#include <QHash>
#include <QObject>
#include <QPair>

typedef QPair< QString, QString > InkPair;



class FORM_EXTRACT_UTILS_EXPORT InkTransformer : public QObject
{

    Q_OBJECT

public:

    InkTransformer( QObject *parent = 0 );
    virtual ~InkTransformer();

    bool transform( const QList< InkPair > inks, const QString &targetDirectory );
    bool isTransforming() const;
    const QHash< InkPair, QString > &transformedFiles() const;

signals:

    void finishedTransforming();

private:

    class InkTransformerPrivate;
    InkTransformerPrivate *d;

    bool startTransformingCurrent();

private slots:

    void finishedTransformingPair( bool, bool );

};

#endif // INKTRANSFORMER_H
