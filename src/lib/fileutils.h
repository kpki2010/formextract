/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILEUTILS_H
#define FILEUTILS_H

#include "formextractutilsdefines.h"

#include <QString>

class FORM_EXTRACT_UTILS_EXPORT FileUtils
{

public:

    static const QString findNonExistingFileName( const QString &dir, const QString &base, const QString &ext );
    static bool fileExists( const QString &fileName );
    static const QString basename( const QString &filename );

private:

    FileUtils() {}
    FileUtils( const FileUtils & ) {}

};

#endif // FILEUTILS_H
