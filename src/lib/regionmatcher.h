/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REGIONMATCHER_H
#define REGIONMATCHER_H

#include "formextractutilsdefines.h"

#include <QHash>
#include <QObject>
#include <QPair>

typedef QPair< QString, QString > RegionPair;

uint FORM_EXTRACT_UTILS_EXPORT qHash( const RegionPair &p );



class FORM_EXTRACT_UTILS_EXPORT RegionMatcher : public QObject
{

    Q_OBJECT

public:

    RegionMatcher( QObject *parent = 0 );
    virtual ~RegionMatcher();

    bool matchRegions( const QList< RegionPair > regions, const QString &targetDirectory );
    bool isMatchingRegions() const;
    const QHash< RegionPair, QString > &matchedFiles() const;

signals:

    void finishedMatchingRegions();

private:

    class RegionMatcherPrivate;
    RegionMatcherPrivate *d;

    bool startMatchingCurrent();

private slots:

    void finishedMatchingRegionPair( bool, bool );

};

#endif // REGIONMATCHER_H
