TEMPLATE = lib
TARGET = formextractutils
CONFIG += plugin
QT += core xml
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release
HEADERS = programrunner.h \
    formextractutilsdefines.h \
    toolsconfiguration.h \
    inkmanager.h \
    fileutils.h \
    imagefilter.h \
    regionextractor.h \
    regionmatcher.h \
    debugmessagehandler.h \
    regionpairoptimizerthread.h \
    inktransformer.h \
    formsconfiguration.h \
    inkrecognizer.h
SOURCES = programrunner.cpp \
    toolsconfiguration.cpp \
    inkmanager.cpp \
    fileutils.cpp \
    imagefilter.cpp \
    regionextractor.cpp \
    regionmatcher.cpp \
    debugmessagehandler.cpp \
    regionpairoptimizerthread.cpp \
    inktransformer.cpp \
    formsconfiguration.cpp \
    inkrecognizer.cpp
DEFINES = FORM_EXTRACT_UTILS
