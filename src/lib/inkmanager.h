/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INKMANAGER_H
#define INKMANAGER_H

#include "formextractutilsdefines.h"

#include <QHash>
#include <QObject>

class FORM_EXTRACT_UTILS_EXPORT InkManager : public QObject
{

    Q_OBJECT

public:

    InkManager( QObject *parent = 0 );
    virtual ~InkManager();

    bool readDevice( const QString &targetDirectory );
    const QStringList &readDeviceFiles() const;
    bool isReadingDevice() const;

    bool clearDevice();

    bool rasterInk( const QStringList &inkFiles, const QString &targetDirectory );
    bool isRasteringInk() const;
    const QHash< QString, QString > &rasteredImages() const;

    bool convertToInkXML( const QStringList &inkFiles, const QString &targetDirectory );
    bool isConvertingToInkXML() const;
    const QHash< QString, QString > &inkXMLFiles() const;

    bool convertToInk( const QStringList &xmlFiles, const QString &targetDirectory );
    bool isConvertingToInk() const;
    const QHash< QString, QString > &inkFiles() const;

signals:

    void deviceRead();
    void deviceCleared();
    void rasteredInk();
    void convertedToInkXML();
    void convertedToInk();

private:

    class InkManagerPrivate;
    InkManagerPrivate *d;

    bool startRasterCurrentInk();
    bool startConvertCurrentToInkXML();
    bool startConvertCurrentToInk();

private slots:

    void readDeviceFinished( bool, bool );
    void clearDeviceFinished( bool, bool );
    void rasterInkFinished( bool, bool );
    void convertToInkXMLFinished( bool, bool );
    void convertToInkFinished( bool, bool );

};

#endif // INKMANAGER_H
