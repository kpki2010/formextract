/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROGRAMRUNNER_H
#define PROGRAMRUNNER_H

#include "formextractutilsdefines.h"

#include <QHash>
#include <QObject>
#include <QProcess>
#include <QStringList>

/**
  * @brief Run other programs and collect their output.
  *
  * The ProgramRunner can be used to run other programs. It is able to run programs
  * sequentially. By calling the run() method multiple times, one can add one or more programs
  * to be run. The runner starts the given program, if currently no other process is running. Otherwise,
  * the program is written to a queue and will be executed later. This behavior can be surpressed by
  * setting the @bold{enqueue} argument to false.
  *
  * Infos about the current state can be read using the various reader methods provided.
  *
  * The processing can be stopped using the stop() method.
  *
  * The runner collects the output of all processes; it can be accessed using the output() method.
  * The output is represented as a list of structured data records, each holding the actual line content and
  * whether the line has been read from standard output or standard error.
  *
  * @author Martin Höher <martin@rpdev.net>
  */
class FORM_EXTRACT_UTILS_EXPORT ProgramRunner : public QObject
{

    Q_OBJECT

public:

    /**
      * @brief Determines the kind of source of a output line.
      */
    typedef enum
    {
        FromStdOut          = 0,                    //!< The line has been read from standard output.
        FromStdErr          = 1                     //!< The line has been read from standard error.
    } OutputChannelType;

    /**
      * @brief Holds information about a line read from a program previously run.
      */
    typedef struct
    {
        OutputChannelType   type;                   //!< Determines from where the line has been read.
        QString             line;                   //!< The line content.
    } ProgramOutputLine;

    /**
      * @brief Default constructor.
      */
    ProgramRunner( QObject *parent = 0 );

    /**
      * @brief Default destructor.
      */
    virtual ~ProgramRunner();

    /**
      * @brief Run or enqueue a program.
      *
      * If the runner is currently idle, the program will be started immediately. Otherwise
      * (i.e. there is already a program running) this will add the program to the runner's
      * queue and started as soon as all previous programs finished.
      *
      * @param program The program to run.
      * @param args The arguments to pass to the program.
      * @param environment Additional entries for the program environment. Holds a key-value list. The entries
      *                    are appended to the current system environment.
      * @param enqueue If true, the program will be written to the queue and executed later,
      *                if the runner is not idle when calling this method. Otherwise, the
      *                data is discarded.
      *
      * @returns True, if the program has been started successfully or enqueued or false otherwise.
      */
    bool run( QString program, const QStringList &args = QStringList(),
              const QHash< QString, QString > environment = QHash< QString, QString >(),
              bool enqueue = true );

    /**
      * @brief Clear output collected from previous runs.
      *
      * This clears all output collected from previous runs.
      */
    void clear();

    /**
      * @brief Stop processing.
      *
      * This method will do two things:
      *
      * <ul>
      *   <li> If a program is currently running, it will be stopped by sending a SIGKILL (on UNIX platforms) or
      *        using TerminateProcess() (on Windows).</li>
      *   <li>The current queue is is cleared.</li>
      * </ul>
      *
      * @returns True if something has been done or false otherwise (i.e. the runner was idle).
      */
    bool stop();

    /**
      * @brief Check whether runner is idle.
      *
      * @returns True, if a program currently runs, otherwise false.
      */
    bool isRunning() const;

    /**
      * @brief Returns the program of the process started most recently.
      */
    const QString &program() const;

    /**
      * @brief Returns the arguments of the process started most recently.
      */
    const QStringList &args() const;

    /**
      * @brief Returns the environment of the process started most recently.
      */
    const QHash< QString, QString > &environment() const;

    /**
      * @brief Returns collected output.
      *
      * This returns the list of output collected from all previous runs (unless cleared
      * using the clear() method).
      */
    const QList< ProgramOutputLine > &output() const;

signals:

    /**
      * @brief A line has been read.
      *
      * This is emitted whenever a new line of output has been read and added to the output.
      */
    void outputReceived( const ProgramOutputLine &line );

    /**
      * @brief A program has finished.
      *
      * This signal is sent to indicate, that a program has finished.
      *
      * @param success True, if the called program finished with success (i.e. exit code 0), otherwise false.
      * @param queueEmpty True, if there are no further programs in the queue (i.e. after that signal, the runner will be idle).
      */
    void finished( bool success, bool queueEmpty );

private:

    class ProgramRunnerPrivate;
    ProgramRunnerPrivate *d;

private slots:

    void readStdOut();
    void readStdErr();
    void processFinished( int exitCode, QProcess::ExitStatus );
    void fakeProcessFinished();

};

#endif // PROGRAMRUNNER_H
