/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionextractor.h"

#include "fileutils.h"
#include "programrunner.h"
#include "toolsconfiguration.h"

#include <QDebug>

class RegionExtractor::RegionExtractorPrivate
{

public:

    QStringList                 imageFiles;
    QString                     targetDirectory;
    QHash< QString, QString >   extractedFiles;
    int                         currentIndex;
    ProgramRunner               *runner;

    RegionExtractorPrivate( RegionExtractor *re )
        : imageFiles( QStringList() ),
          targetDirectory( QString() ),
          extractedFiles( QHash< QString, QString >() ),
          currentIndex( -1 ),
          runner( new ProgramRunner( re ) ),
          regionExtractor( re )
    {
    }

    virtual ~RegionExtractorPrivate()
    {
    }

private:

    RegionExtractor *regionExtractor;

};

RegionExtractor::RegionExtractor( QObject *parent )
    : QObject( parent ),
      d( new RegionExtractorPrivate( this ) )
{
    connect( d->runner, SIGNAL(finished(bool,bool)), this, SLOT(finishedExtractingRegion(bool,bool)) );
}

RegionExtractor::~RegionExtractor()
{
    delete d;
}

bool RegionExtractor::extractRegions( const QStringList &imageFiles, const QString &targetDirectory )
{
    if ( !( isExtractingRegions() ) || imageFiles.length() > 0 )
    {
        d->currentIndex = 0;
        d->extractedFiles.clear();
        d->imageFiles = imageFiles;
        d->targetDirectory = targetDirectory;

        return startExtractCurrentRegion();
    } else
    {
        return false;
    }
}

bool RegionExtractor::isExtractingRegions() const
{
    return d->currentIndex >= 0;
}

const QHash< QString, QString > &RegionExtractor::extractedRegions() const
{
    return d->extractedFiles;
}

bool RegionExtractor::startExtractCurrentRegion()
{
    QString input = d->imageFiles[ d->currentIndex ];
    QString output = FileUtils::findNonExistingFileName( d->targetDirectory,
                                                         FileUtils::basename( input ),
                                                         ".xml" );
    d->extractedFiles.insert( input, output );
    QStringList args;
    args << "--input" << input << "--output" << output << "--format" << "xml";
    qDebug() << "Start extracting reagions...";
    qDebug() << "Arguments: " << args;
    return d->runner->run( ToolsConfiguration::instance()->regionExtractor(), args );
}

void RegionExtractor::finishedExtractingRegion( bool, bool )
{
    d->currentIndex++;
    if ( d->currentIndex < d->imageFiles.length() )
    {
        startExtractCurrentRegion();
    } else
    {
        d->currentIndex = -1;
        d->imageFiles.clear();
        d->targetDirectory = QString();
        emit finishedExtractingRegions();
    }
}
