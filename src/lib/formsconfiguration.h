/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FORMSCONFIGURATION_H
#define FORMSCONFIGURATION_H

#include "formextractutilsdefines.h"

#include <QObject>
#include <QRect>
#include <QStringList>

class FORM_EXTRACT_UTILS_EXPORT FormsConfiguration : public QObject
{

    Q_OBJECT

public:

    static FormsConfiguration *instance();

    const QRect &formRect() const;
    void setFormRect( const QRect &fr );

    const QStringList &forms() const;
    void setForms( const QStringList &f );

signals:

public slots:

private:

    FormsConfiguration(QObject *parent = 0);

    FormsConfiguration( const FormsConfiguration & ) : QObject()
    {
    }

    class FormsConfigurationPrivate;
    FormsConfigurationPrivate *d;

private slots:

    void saveSettings();
    void restoreSettings();

};

#endif // FORMSCONFIGURATION_H
