/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inkrecognizer.h"

#include "fileutils.h"
#include "programrunner.h"
#include "toolsconfiguration.h"

#include <QDebug>


class InkRecognizer::InkRecognizerPrivate
{

public:

    QList< InkFormPair >            pairs;
    QString                         targetDirectory;
    QHash< InkFormPair, QString >   readFiles;
    int                             currentIndex;
    ProgramRunner                  *runner;



    InkRecognizerPrivate( InkRecognizer *ir )
        : pairs( QList< InkFormPair >() ),
           targetDirectory( QString() ),
           readFiles( QHash< InkFormPair, QString >() ),
           currentIndex( -1 ),
           runner( new ProgramRunner( ir ) ),
           inkRecognizer( ir )
    {
    }

    virtual ~InkRecognizerPrivate()
    {
    }

private:

    InkRecognizer *inkRecognizer;

};

InkRecognizer::InkRecognizer( QObject *parent )
    : QObject( parent ),
      d( new InkRecognizerPrivate( this ) )
{
    connect( d->runner, SIGNAL(finished(bool,bool)), this, SLOT(finishedReadingInk(bool,bool)) );
}

InkRecognizer::~InkRecognizer()
{
    delete d;
}

bool InkRecognizer::read( QList<InkFormPair> inks, const QString &targetDirectory )
{
    if ( isReading() || inks.length() == 0 )
    {
        return false;
    } else
    {
        d->pairs = inks;
        d->targetDirectory = targetDirectory;
        d->currentIndex = 0;
        d->readFiles.clear();
        return startReadCurrent();
    }
}

bool InkRecognizer::isReading() const
{
    return d->currentIndex >= 0;
}

const QHash< InkFormPair, QString > &InkRecognizer::readFiles() const
{
    return d->readFiles;
}

bool InkRecognizer::startReadCurrent()
{
    InkFormPair input = d->pairs[ d->currentIndex ];
    QString output = FileUtils::findNonExistingFileName( d->targetDirectory, "read-ink", ".xml" );
    d->readFiles.insert( input, output );
    QStringList args;
    args << "-ink" << input.first << "-formular" << input.second << "-target" << output;
    qDebug() << "Start reading Ink file...";
    qDebug() << "Arguments: " << args;
    return d->runner->run( ToolsConfiguration::instance()->inkRecognizer(), args );
}

void InkRecognizer::finishedReadingInk( bool, bool )
{
    d->currentIndex++;
    if ( d->currentIndex < d->pairs.length() )
    {
        startReadCurrent();
    } else
    {
        d->currentIndex = -1;
        d->pairs.clear();
        d->targetDirectory = QString();
        emit finishedReading();
    }
}
