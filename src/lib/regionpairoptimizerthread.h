/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REGIONPAIROPTIMIZERTHREAD_H
#define REGIONPAIROPTIMIZERTHREAD_H

#include "regionmatcher.h"

#include "formextractutilsdefines.h"

#include <QHash>
#include <QThread>

class FORM_EXTRACT_UTILS_EXPORT RegionPairOptimizerThread : public QThread
{
    Q_OBJECT
public:

    explicit RegionPairOptimizerThread(QObject *parent = 0);
    virtual ~RegionPairOptimizerThread();

    void setMatchingResults( QHash< RegionPair, QString > results );
    const QList< RegionPair > &pairs() const;

    void run();

signals:

public slots:

private:

    class RegionPairOptimizerThreadPrivate;
    RegionPairOptimizerThreadPrivate *d;

};

#endif // REGIONPAIROPTIMIZERTHREAD_H
