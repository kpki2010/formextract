/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOOLSCONFIGURATION_H
#define TOOLSCONFIGURATION_H

#include "formextractutilsdefines.h"

#include <QObject>

class FORM_EXTRACT_UTILS_EXPORT ToolsConfiguration : public QObject
{

    Q_OBJECT

public:

    ToolsConfiguration( QObject *parent = 0 );
    virtual ~ToolsConfiguration();

    void saveSettings() const;
    void restoreSettings();

    static ToolsConfiguration *instance();

    const QString &imageFilter() const;
    void setImageFilter( const QString &imf );

    const QString &inkManager() const;
    void setInkManager( const QString &im );

    const QString &regionExtractor() const;
    void setRegionExtractor( const QString &re );

    const QString &regionMatcher() const;
    void setRegionMatcher( const QString &rm );

    const QString &inkTransformer() const;
    void setInkTransformer( const QString &it );

    const QString &inkRecognizer() const;
    void setInkRecognizer( const QString &ir );

private:

    class ToolsConfigurationPrivate;
    ToolsConfigurationPrivate *d;

private slots:

    void onApplicationQuit();

};

#endif // TOOLSCONFIGURATION_H
