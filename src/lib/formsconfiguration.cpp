/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "formsconfiguration.h"

#include <QCoreApplication>
#include <QDebug>
#include <QSettings>


class FormsConfiguration::FormsConfigurationPrivate
{

public:

    QRect       FormRect;
    QStringList Forms;

    FormsConfigurationPrivate() :
            FormRect( QRect( 50, 50, 700, 910 ) ),
            Forms( QStringList() )
    {
    }

    virtual ~FormsConfigurationPrivate()
    {
    }

};

FormsConfiguration *FormsConfiguration::instance()
{
    static FormsConfiguration *i;
    if ( !( i ) )
    {
        i = new FormsConfiguration();
    }
    return i;
}

const QRect &FormsConfiguration::formRect() const
{
    return d->FormRect;
}

void FormsConfiguration::setFormRect( const QRect &fr )
{
    d->FormRect = fr;
}

const QStringList &FormsConfiguration::forms() const
{
    return d->Forms;
}

void FormsConfiguration::setForms( const QStringList &f )
{
    d->Forms = f;
}

FormsConfiguration::FormsConfiguration(QObject *parent) :
    QObject(parent),
    d( new FormsConfigurationPrivate() )
{
    connect( QCoreApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(saveSettings()) );
    restoreSettings();
}

void FormsConfiguration::saveSettings()
{
    QSettings settings;
    settings.beginGroup( "FormsConfiguration" );
    settings.setValue( "Rect", d->FormRect );
    settings.setValue( "Forms", d->Forms );
    settings.endGroup();
    qDebug() << "FormsConfiguration: Saved settings!";
}

void FormsConfiguration::restoreSettings()
{
    QSettings settings;
    settings.beginGroup( "FormsConfiguration" );
    d->FormRect = settings.value( "Rect", d->FormRect ).toRect();
    d->Forms = settings.value( "Forms", d->Forms ).toStringList();
    settings.endGroup();
    settings.sync();
    qDebug() << "FormsConfiguration: Restored Settings!";
}
