/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "toolsconfiguration.h"

#include <QApplication>
#include <QDebug>
#include <QSettings>

class ToolsConfiguration::ToolsConfigurationPrivate
{

public:


    QString ImageFilter;
    QString InkManager;
    QString RegionExtractor;
    QString RegionMatcher;
    QString InkTransformer;
    QString InkRecognizer;

    ToolsConfigurationPrivate( ToolsConfiguration *tc )
        : ImageFilter( "imagefilter" ),
          InkManager( "InkManager" ),
          RegionExtractor( "img2roi" ),
          RegionMatcher( "matcher" ),
          InkTransformer( "inktransformer" ),
          InkRecognizer( "InkRecognizer" ),
          toolsConfiguration( tc )
    {
    }

    virtual ~ToolsConfigurationPrivate()
    {
    }

private:

    ToolsConfiguration *toolsConfiguration;

};

ToolsConfiguration::ToolsConfiguration( QObject *parent )
    : QObject( parent ),
      d( new ToolsConfigurationPrivate( this ) )
{
    restoreSettings();
    connect( qApp, SIGNAL(aboutToQuit()), this, SLOT(onApplicationQuit()) );
}

ToolsConfiguration::~ToolsConfiguration()
{
    delete d;
}

void ToolsConfiguration::saveSettings() const
{
    QSettings settings;
    settings.beginGroup( "Toolsconfiguration" );
    settings.setValue( "InkManager", d->InkManager );
    settings.setValue( "ImageFilter", d->ImageFilter );
    settings.setValue( "RegionExtractor", d->RegionExtractor );
    settings.setValue( "RegionMatcher", d->RegionMatcher );
    settings.setValue( "InkTransformer", d->InkTransformer );
    settings.setValue( "InkRecognizer", d->InkRecognizer );
    settings.endGroup();
    settings.sync();
    qDebug() << "ToolsConfiguration: Saved settings!";
}

void ToolsConfiguration::restoreSettings()
{
    QSettings settings;
    settings.beginGroup("ToolsConfiguration" );
    d->InkManager = settings.value( "InkManager", d->InkManager ).toString();
    d->ImageFilter = settings.value( "ImageFilter", d->ImageFilter ).toString();
    d->RegionExtractor = settings.value( "RegionExtractor", d->RegionExtractor ).toString();
    d->RegionMatcher = settings.value( "RegionMatcher", d->RegionMatcher ).toString();
    d->InkTransformer = settings.value( "InkTransformer", d->InkTransformer ).toString();
    d->InkRecognizer = settings.value( "InkRecognizer", d->InkRecognizer ).toString();
    settings.endGroup();
    qDebug() << "ToolsConfiguration: Restored settings!";
}

ToolsConfiguration *ToolsConfiguration::instance()
{
    static ToolsConfiguration *i;
    if ( !( i ) )
    {
        i = new ToolsConfiguration();
    }
    return i;
}

const QString &ToolsConfiguration::imageFilter() const
{
    return d->ImageFilter;
}

void ToolsConfiguration::setImageFilter( const QString &imf )
{
    d->ImageFilter = imf;
}

const QString &ToolsConfiguration::inkManager() const
{
    return d->InkManager;
}

void ToolsConfiguration::setInkManager( const QString &im )
{
    d->InkManager = im;
}

const QString &ToolsConfiguration::regionExtractor() const
{
    return d->RegionExtractor;
}

void ToolsConfiguration::setRegionExtractor( const QString &re )
{
    d->RegionExtractor = re;
}

const QString &ToolsConfiguration::regionMatcher() const
{
    return d->RegionMatcher;
}

void ToolsConfiguration::setRegionMatcher( const QString &rm )
{
    d->RegionMatcher = rm;
}

const QString &ToolsConfiguration::inkTransformer() const
{
    return d->InkTransformer;
}

void ToolsConfiguration::setInkTransformer( const QString &it )
{
    d->InkTransformer = it;
}

const QString &ToolsConfiguration::inkRecognizer() const
{
    return d->InkRecognizer;
}

void ToolsConfiguration::setInkRecognizer( const QString &ir )
{
    d->InkRecognizer = ir;
}

void ToolsConfiguration::onApplicationQuit()
{
    saveSettings();
}
