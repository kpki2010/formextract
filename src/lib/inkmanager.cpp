/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inkmanager.h"

#include "fileutils.h"
#include "programrunner.h"
#include "toolsconfiguration.h"

#include <QDebug>

class InkManager::InkManagerPrivate
{

public:

    QString                     readDeviceTargetDirectory;
    int                         readDeviceCurrentIndex;
    ProgramRunner              *readDeviceRunner;
    QStringList                 readDeviceFiles;

    ProgramRunner              *clearDeviceRunner;

    QStringList                 rasterInkFiles;
    QHash< QString, QString >   rasterInkMap;
    QString                     rasterInkTargetDirectory;
    int                         rasterInkCurrentIndex;
    ProgramRunner              *rasterInkRunner;

    QStringList                 convertToInkXMLFiles;
    QHash< QString, QString >   convertToInkXMLMap;
    QString                     convertToInkXMLTargetDirectory;
    int                         convertToInkXMLCurrentIndex;
    ProgramRunner              *convertToInkXMLRunner;

    QStringList                 convertToInkFiles;
    QHash< QString, QString >   convertToInkMap;
    QString                     convertToInkTargetDirectory;
    int                         convertToInkCurrentIndex;
    ProgramRunner              *convertToInkRunner;

    InkManagerPrivate( InkManager *im )
        : readDeviceTargetDirectory( QString() ),
          readDeviceCurrentIndex( -1 ),
          readDeviceRunner( new ProgramRunner( im ) ),
          readDeviceFiles(),
          clearDeviceRunner( new ProgramRunner( im ) ),
          rasterInkFiles( QStringList() ),
          rasterInkMap( QHash< QString, QString >() ),
          rasterInkTargetDirectory( QString() ),
          rasterInkCurrentIndex( -1 ),
          rasterInkRunner( new ProgramRunner( im ) ),
          convertToInkXMLFiles( QStringList() ),
          convertToInkXMLMap( QHash< QString, QString >() ),
          convertToInkXMLTargetDirectory( QString() ),
          convertToInkXMLCurrentIndex( -1 ),
          convertToInkXMLRunner( new ProgramRunner( im ) ),
          convertToInkFiles( QStringList() ),
          convertToInkMap( QHash< QString, QString >() ),
          convertToInkTargetDirectory( QString() ),
          convertToInkCurrentIndex( -1 ),
          convertToInkRunner( new ProgramRunner( im ) ),
          inkManager( im )
    {
    }

    virtual ~InkManagerPrivate()
    {
    }

private:

    InkManager *inkManager;

};

InkManager::InkManager( QObject *parent )
    : QObject( parent ),
      d( new InkManagerPrivate( this ) )
{
    connect( d->readDeviceRunner, SIGNAL(finished(bool,bool)), this, SLOT(readDeviceFinished(bool,bool)) );
    connect( d->clearDeviceRunner, SIGNAL(finished(bool,bool)), this, SLOT(clearDeviceFinished(bool,bool)) );
    connect( d->rasterInkRunner, SIGNAL(finished(bool,bool)), this, SLOT(rasterInkFinished(bool,bool)) );
    connect( d->convertToInkXMLRunner, SIGNAL(finished(bool,bool)), this, SLOT(convertToInkXMLFinished(bool,bool)) );
    connect( d->convertToInkRunner, SIGNAL(finished(bool,bool)), this, SLOT(convertToInkFinished(bool,bool)) );
}

InkManager::~InkManager()
{
    delete d;
}

bool InkManager::readDevice( const QString &targetDirectory )
{
    if ( !( isReadingDevice() ) )
    {
        d->readDeviceTargetDirectory = targetDirectory;
        d->readDeviceFiles.clear();
        d->readDeviceCurrentIndex = 0;

        d->readDeviceFiles << FileUtils::findNonExistingFileName( targetDirectory, "ink-", ".ink" );
        QStringList args;
        args << "-source" << "device" << QString( "%1" ).arg( d->readDeviceCurrentIndex ) << "-target" << d->readDeviceFiles.last();
        return d->readDeviceRunner->run( ToolsConfiguration::instance()->inkManager(), args );
    } else
    {
        return false;
    }
}

const QStringList &InkManager::readDeviceFiles() const
{
    return d->readDeviceFiles;
}

bool InkManager::isReadingDevice() const
{
    return d->readDeviceCurrentIndex >= 0;
}

bool InkManager::clearDevice()
{
    if ( !( d->clearDeviceRunner->isRunning() ) )
    {
        QStringList args;
        args << "-deleteFromDevice";
        return d->clearDeviceRunner->run( ToolsConfiguration::instance()->inkManager(), args );
    } else
    {
        return false;
    }
}

bool InkManager::rasterInk( const QStringList &inkFiles, const QString &targetDirectory )
{
    if ( !( isRasteringInk() ) || inkFiles.length() == 0 )
    {
        d->rasterInkCurrentIndex = 0;
        d->rasterInkFiles = inkFiles;
        d->rasterInkMap.clear();
        d->rasterInkTargetDirectory = targetDirectory;

        return startRasterCurrentInk();
    } else
    {
        return false;
    }
}

bool InkManager::isRasteringInk() const
{
    return d->rasterInkCurrentIndex >= 0;
}

const QHash< QString, QString > &InkManager::rasteredImages() const
{
    return d->rasterInkMap;
}

bool InkManager::convertToInkXML( const QStringList &inkFiles, const QString &targetDirectory )
{
    if ( !( isConvertingToInkXML() ) || inkFiles.length() == 0 )
    {
        d->convertToInkXMLCurrentIndex = 0;
        d->convertToInkXMLFiles = inkFiles;
        d->convertToInkXMLMap.clear();
        d->convertToInkXMLTargetDirectory = targetDirectory;

        return startConvertCurrentToInkXML();
    } else
    {
        return false;
    }
}

bool InkManager::isConvertingToInkXML() const
{
    return d->convertToInkXMLCurrentIndex >= 0;
}

const QHash< QString, QString > &InkManager::inkXMLFiles() const
{
    return d->convertToInkXMLMap;
}

bool InkManager::convertToInk(const QStringList &xmlFiles, const QString &targetDirectory )
{
    if ( !( isConvertingToInk() ) || xmlFiles.length() == 0 )
    {
        d->convertToInkCurrentIndex = 0;
        d->convertToInkFiles = xmlFiles;
        d->convertToInkMap.clear();
        d->convertToInkTargetDirectory = targetDirectory;

        return startConvertCurrentToInk();
    } else
    {
        return false;
    }
}

bool InkManager::isConvertingToInk() const
{
    return d->convertToInkCurrentIndex >= 0;
}

const QHash< QString, QString > &InkManager::inkFiles() const
{
    return d->convertToInkMap;
}

bool InkManager::startRasterCurrentInk()
{
    QString input = d->rasterInkFiles[ d->rasterInkCurrentIndex ];
    QString currentFile = FileUtils::findNonExistingFileName( d->rasterInkTargetDirectory,
                                                              FileUtils::basename( input ),
                                                              ".bmp" );
    d->rasterInkMap.insert( input, currentFile );
    QStringList args;
    args << "-source" << input
         << "-target" << currentFile;
    qDebug() << "Start rendering Ink to image...";
    qDebug() << "Arguments: " << args;
    return d->rasterInkRunner->run( ToolsConfiguration::instance()->inkManager(), args );
}

bool InkManager::startConvertCurrentToInkXML()
{
    QString input = d->convertToInkXMLFiles[ d->convertToInkXMLCurrentIndex ];
    QString currentFile = FileUtils::findNonExistingFileName( d->convertToInkXMLTargetDirectory,
                                                              FileUtils::basename( input ),
                                                              ".xml" );
    d->convertToInkXMLMap.insert( input, currentFile );
    QStringList args;
    args << "-source" << input
         << "-target" << currentFile;
    qDebug() << "Start converting Ink to XML...";
    qDebug() << "Arguments: " << args;
    return d->convertToInkXMLRunner->run( ToolsConfiguration::instance()->inkManager(), args );
}

bool InkManager::startConvertCurrentToInk()
{
    QString input = d->convertToInkFiles[ d->convertToInkCurrentIndex ];
    QString currentFile = FileUtils::findNonExistingFileName( d->convertToInkTargetDirectory,
                                                              FileUtils::basename( input ),
                                                              ".ink" );
    d->convertToInkMap.insert( input, currentFile );
    QStringList args;
    args << "-source" << input
         << "-target" << currentFile;
    qDebug() << "Start converting XML to Ink...";
    qDebug() << "Arguments: " << args;
    return d->convertToInkRunner->run( ToolsConfiguration::instance()->inkManager(), args );
}

void InkManager::readDeviceFinished( bool, bool )
{
    if ( FileUtils::fileExists( d->readDeviceFiles.last() ) )
    {
        d->readDeviceCurrentIndex++;
        d->readDeviceFiles << FileUtils::findNonExistingFileName( d->readDeviceTargetDirectory, "ink-", ".ink" );
        QStringList args;
        args << "-source" << "device" << QString( "%1" ).arg( d->readDeviceCurrentIndex ) << "-target" << d->readDeviceFiles.last();
        d->readDeviceRunner->run( ToolsConfiguration::instance()->inkManager(), args );
    } else
    {
        d->readDeviceFiles.removeLast();
        d->readDeviceCurrentIndex = -1;
        d->readDeviceTargetDirectory = QString();
        emit deviceRead();
    }
}

void InkManager::clearDeviceFinished( bool, bool )
{
    emit deviceCleared();
}

void InkManager::rasterInkFinished( bool, bool )
{
    d->rasterInkCurrentIndex++;
    if ( d->rasterInkCurrentIndex < d->rasterInkFiles.length() )
    {
        startRasterCurrentInk();
    } else
    {
        d->rasterInkCurrentIndex = -1;
        d->rasterInkFiles.clear();
        d->rasterInkTargetDirectory = QString();
        emit rasteredInk();
    }
}

void InkManager::convertToInkXMLFinished( bool, bool )
{
    d->convertToInkXMLCurrentIndex++;
    if ( d->convertToInkXMLCurrentIndex < d->convertToInkXMLFiles.length() )
    {
        startConvertCurrentToInkXML();
    } else
    {
        d->convertToInkXMLCurrentIndex = -1;
        d->convertToInkXMLFiles.clear();
        d->convertToInkXMLTargetDirectory = QString();
        emit convertedToInkXML();
    }
}

void InkManager::convertToInkFinished( bool, bool )
{
    d->convertToInkCurrentIndex++;
    if ( d->convertToInkCurrentIndex < d->convertToInkFiles.length() )
    {
        startConvertCurrentToInk();
    } else
    {
        d->convertToInkCurrentIndex = -1;
        d->convertToInkFiles.clear();
        d->convertToInkTargetDirectory = QString();
        emit convertedToInk();
    }
}
