/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "regionpairoptimizerthread.h"

#include <QDomDocument>
#include <QFile>
#include <qmath.h>

#include <stdlib.h>

#define PENALTY 1000.0

class RegionPairOptimizerThread::RegionPairOptimizerThreadPrivate
{

public:

    typedef struct
    {
        QList< RegionPair > pairs;
        QList< QString > firsts;
        QList< QString > seconds;
    } State;

    QHash< RegionPair, QString > matchingResults;
    QHash< RegionPair, double > matchingDifferences;
    double differenceMax;

    QList< RegionPair > pairs;
    QList< QString > firsts;
    QList< QString > seconds;

    RegionPairOptimizerThreadPrivate( RegionPairOptimizerThread *t ) :
            thread( t )
    {
    }

    virtual ~RegionPairOptimizerThreadPrivate()
    {
    }

    /*
     Find optimal list of pairs using simulated annealing.
     */
    void saOptimize()
    {
        splitPairs();
        loadDifferencesFromFile();
        createInitialPairings();
        double currentEnergy = energy( pairs );
        int timeMax = ( firsts.length() + seconds.length() + pairs.length() ) * 100 + 1;
        int time = 1;
        while ( time <= timeMax )
        {
            State newState = nextState();
            double newEnergy = energy( newState.pairs );
            double decision = qrand() / (double) RAND_MAX;
            double prob = probability( newEnergy, currentEnergy, time, timeMax );
            if ( decision <= prob )
            {
                pairs = newState.pairs;
                firsts = newState.firsts;
                seconds = newState.seconds;
                currentEnergy = newEnergy;
            }
            time++;
        }
    }

private:

    /*
     Splits the matching results into two separate lists with one
     holding the first component of each pair and the second holding the
     second components.
     */
    void splitPairs()
    {
        firsts.clear();
        seconds.clear();
        foreach ( RegionPair pair, matchingResults.keys() )
        {
            if ( !( firsts.contains( pair.first ) ) )
            {
                firsts.append( pair.first );
            }

            if ( !( seconds.contains( pair.second ) ) )
            {
                seconds.append( pair.second );
            }
        }
    }

    /*
     Load the differences (deviations) from matching result files.
     */
    void loadDifferencesFromFile()
    {
        differenceMax = 0;
        matchingDifferences.clear();
        foreach ( RegionPair pair, matchingResults.keys() )
        {
            double difference = differenceFromFile( matchingResults.value( pair ) );
            matchingDifferences.insert( pair, difference );
            if ( differenceMax == PENALTY || differenceMax < difference )
            {
                differenceMax = difference;
            }
        }
    }

    /*
     Reads the deviation from a single XML file and returns it.
     */
    double differenceFromFile( const QString &filename )
    {
        QFile file( filename );
        if ( file.open( QIODevice::ReadOnly ) )
        {
            QDomDocument doc;
            QByteArray fileData = file.readAll();
            file.close();
            if ( doc.setContent( fileData ) )
            {
                QDomNodeList deviations = doc.elementsByTagName( "deviation" );
                if ( deviations.length() > 0 )
                {
                    QDomElement deviation = deviations.at( 0 ).toElement();
                    if ( !( deviation.isNull() ) )
                    {
                        bool ok;
                        QString d = deviation.text();
                        double result = d.toDouble( &ok );
                        if ( ok )
                        {
                            return result;
                        }
                    }
                }
            }
        }
        return PENALTY;
    }

    /*
     Generates a start solution.
     */
    void createInitialPairings()
    {
       pairs.clear();
       foreach ( QString first, firsts )
       {
           QString selected;
           double sdifference = PENALTY;
           foreach ( QString second, seconds )
           {
               double difference = matchingDifferences.value( RegionPair( first, second ), PENALTY );
               if ( difference < sdifference || selected.isNull() )
               {
                    selected = second;
                    sdifference = difference;
               }
           }
           if ( !( selected.isNull() ) )
           {
               pairs.append( RegionPair( first, selected ) );
               firsts.removeAll( first );
               seconds.removeAll( selected );
           }
       }
    }

    /*
     Returns the "energy" of a list if pairs.
     The energy is the average relative deviation of the pairs.
     */
    double energy( QList< RegionPair > pairs )
    {
        double result = 0.0;
        foreach ( RegionPair pair, pairs )
        {
            result += matchingDifferences.value( pair, PENALTY ) / ( differenceMax != 0.0 ? differenceMax : 1.0 );
        }
        result /= ( pairs.length() > 0 ? pairs.length() : 1.0 );
        return result;
    }

    /*
     Calculates the probability for accespting the new state (with the energy energyNew).
     This is 1.0 if the new state has lower energy; otherwise, the probability depends on
     the difference of energies between the old and new state and the current time.
     */
    double probability( double energyNew, double energyOld, double time, double timeMax )
    {
        if ( energyNew <= energyOld )
        {
            return 1.0;
        } else if ( time == 0 || timeMax == 0 )
        {
            return 0.0;
        } else
        {
            double k = ( differenceMax == 0.0 ? 1.0 : 1 / differenceMax );
            return 1.0 - exp( ( energyOld - energyNew) * k * ( 1 - ( time / timeMax ) ) );
        }
    }

    /*
     Creates a mutation of the current state.
     Internally, this does one of two things:
     Either, a pair is selected and one of its components is swapped with one of the yet
     unbound elements or two pairs are selected and their components are exchanged.
     The probability for each of the two mutations is proportional to the
     size of the available lists (i.e. the more pairs there are compared to the number of
     unbound values, the more likely it is that pairs are exchanged).
     */
    State nextState()
    {
        State result;
        result.pairs = pairs;
        result.firsts = firsts;
        result.seconds = seconds;

        if ( ( firsts.length() + seconds.length() + pairs.length() ) > 0 )
        {
            int what = qrand() % ( firsts.length() + seconds.length() + pairs.length() );

            if ( what >= 0 && what < firsts.length() )
            {
                int idx = qrand() % pairs.length();
                RegionPair pair = pairs.at( idx );
                result.pairs.removeAt( idx );
                QString selected;
                double sdifference;
                foreach ( QString first, firsts )
                {
                    double difference = matchingDifferences.value( RegionPair( first, pair.second ), PENALTY );
                    if ( difference < sdifference || selected.isNull() )
                    {
                        selected = first;
                        sdifference = difference;
                    }
                }
                result.firsts.removeAll( selected );
                result.firsts.append( pair.first );
                result.pairs.append( RegionPair( selected, pair.second ) );
            } else if ( what >= firsts.length() && what < ( firsts.length() + seconds.length() ) )
            {
                int idx = qrand() % pairs.length();
                RegionPair pair = pairs.at( idx );
                result.pairs.removeAt( idx );
                QString selected;
                double sdifference;
                foreach ( QString second, seconds )
                {
                    double difference = matchingDifferences.value( RegionPair( pair.first, second), PENALTY );
                    if ( difference < sdifference || selected.isNull() )
                    {
                        selected = second;
                        sdifference = difference;
                    }
                }
                result.seconds.removeAll( selected );
                result.seconds.append( pair.second );
                result.pairs.append( RegionPair( pair.first, selected ) );
            } else
            {
                if ( result.pairs.length() > 2 )
                {
                    int idx = qrand() % result.pairs.length();
                    RegionPair firstPair = result.pairs.at( idx );
                    result.pairs.removeAt( idx );
                    idx = qrand() % result.pairs.length();
                    RegionPair secondPair = result.pairs.at( idx );
                    result.pairs.removeAt( idx );
                    result.pairs << RegionPair( firstPair.first, secondPair.second )
                                 << RegionPair( secondPair.first, firstPair.second );
                }
            }
        }

        return result;
    }

    RegionPairOptimizerThread *thread;

};

RegionPairOptimizerThread::RegionPairOptimizerThread(QObject *parent) :
    QThread(parent),
    d( new RegionPairOptimizerThreadPrivate( this ) )
{
}

RegionPairOptimizerThread::~RegionPairOptimizerThread()
{
    delete d;
}

void RegionPairOptimizerThread::setMatchingResults( QHash<RegionPair, QString> results )
{
    d->matchingResults = results;
}

const QList< RegionPair > &RegionPairOptimizerThread::pairs() const
{
    return d->pairs;
}

void RegionPairOptimizerThread::run()
{
    d->saOptimize();
}
