/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "programrunner.h"

#include <QDebug>
#include <QProcessEnvironment>
#include <QTimer>



class ProgramRunner::ProgramRunnerPrivate
{

public:

    typedef struct
    {
        QString program;
        QStringList args;
        QHash< QString, QString > environment;
    } ProgramRunInfo;

    QProcess                    *process;
    QList< ProgramRunInfo >      queue;
    QList< ProgramOutputLine >   output;
    QString                      program;
    QStringList                  arguments;
    QHash< QString, QString >    environment;


    ProgramRunnerPrivate( ProgramRunner *r )
        : process( new QProcess( r ) ),
          queue(),
          output(),
          program(),
          arguments(),
          environment(),
          runner( r )
    {
    }

    virtual ~ProgramRunnerPrivate()
    {
    }

private:

    ProgramRunner *runner;

};


ProgramRunner::ProgramRunner( QObject *parent )
    : QObject( parent ),
      d( new ProgramRunnerPrivate( this ) )
{
    connect( d->process, SIGNAL(readyReadStandardOutput()), this, SLOT(readStdOut()) );
    connect( d->process, SIGNAL(readyReadStandardError()), this, SLOT(readStdErr()) );
    connect( d->process, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(processFinished(int,QProcess::ExitStatus)) );
}

ProgramRunner::~ProgramRunner()
{
    delete d;
}

bool ProgramRunner::run( QString program, const QStringList &args, QHash< QString, QString > environment, bool enqueue )
{
    if ( !( isRunning() ) )
    {
        d->program = program;
        d->arguments = args;
        d->environment = environment;
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        foreach ( QString key, environment.keys() )
        {
            env.insert( key, environment.value( key ) );
        }
        d->process->setProcessEnvironment( env );
        d->process->start( program, args );
        if ( !( d->process->waitForStarted() ) )
        {
            QTimer::singleShot( 0, this, SLOT(fakeProcessFinished()) );
            return false;
        } else
        {
            return true;
        }
    } else
    {
        if ( enqueue )
        {
            ProgramRunnerPrivate::ProgramRunInfo info;
            info.program = program;
            info.args = args;
            info.environment = environment;
            d->queue.append( info );
            return true;
        } else
        {
            return false;
        }
    }
}

void ProgramRunner::clear()
{
    d->output.clear();
}

bool ProgramRunner::stop()
{
    if ( isRunning() )
    {
        d->process->kill();
        d->queue.clear();
        return true;
    }
    return false;
}

bool ProgramRunner::isRunning() const
{
    return d->process->state() != QProcess::NotRunning;
}

const QString &ProgramRunner::program() const
{
    return d->program;
}

const QStringList &ProgramRunner::args() const
{
    return d->arguments;
}

const QHash< QString, QString > &ProgramRunner::environment() const
{
    return d->environment;
}

const QList< ProgramRunner::ProgramOutputLine > &ProgramRunner::output() const
{
    return d->output;
}

void ProgramRunner::readStdOut()
{
    d->process->setReadChannel( QProcess::StandardOutput );
    while ( d->process->canReadLine() )
    {
        ProgramOutputLine line;
        line.type = FromStdOut;
        line.line = d->process->readLine();
        d->output.append( line );
        qDebug() << line.line;
        emit outputReceived( d->output.last() );
    }
}

void ProgramRunner::readStdErr()
{
    d->process->setReadChannel( QProcess::StandardError );
    while ( d->process->canReadLine() )
    {
        ProgramOutputLine line;
        line.type = FromStdErr;
        line.line = d->process->readLine();
        d->output.append( line );
        qWarning() << line.line;
        emit outputReceived( d->output.last() );
    }
}

void ProgramRunner::processFinished( int exitCode, QProcess::ExitStatus )
{
    emit finished( exitCode == 0, d->queue.isEmpty() );
    if ( !( d->queue.isEmpty() ) )
    {
        ProgramRunnerPrivate::ProgramRunInfo info = d->queue.first();
        d->queue.removeFirst();
        d->program = info.program;
        d->arguments = info.args;
        d->environment = info.environment;
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        foreach ( QString key, info.environment.keys() )
        {
            env.insert( key, info.environment.value( key ) );
        }
        d->process->setProcessEnvironment( env );
        d->process->start( info.program, info.args );
        if ( !( d->process->waitForStarted() ) )
        {
            QTimer::singleShot( 0, this, SLOT(fakeProcessFinished()) );
        }
    }
}

void ProgramRunner::fakeProcessFinished()
{
    processFinished( 1, QProcess::CrashExit );
}
