/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inktransformer.h"

#include "fileutils.h"
#include "programrunner.h"
#include "toolsconfiguration.h"

#include <QDebug>


class InkTransformer::InkTransformerPrivate
{

public:

    QList< InkPair >                pairs;
    QString                         targetDirectory;
    QHash< InkPair, QString >       transformedFiles;
    int                             currentIndex;
    ProgramRunner                  *runner;



    InkTransformerPrivate( InkTransformer *it )
        : pairs( QList< InkPair >() ),
           targetDirectory( QString() ),
           transformedFiles( QHash< InkPair, QString >() ),
           currentIndex( -1 ),
           runner( new ProgramRunner( it ) ),
           inkTransformer( it )
    {
    }

    virtual ~InkTransformerPrivate()
    {
    }

private:

    InkTransformer *inkTransformer;

};

InkTransformer::InkTransformer( QObject *parent )
    : QObject( parent ),
      d( new InkTransformerPrivate( this ) )
{
    connect( d->runner, SIGNAL(finished(bool,bool)), this, SLOT(finishedTransformingPair(bool,bool)) );
}

InkTransformer::~InkTransformer()
{
    delete d;
}

bool InkTransformer::transform( QList<InkPair>inks, const QString &targetDirectory )
{
    if ( isTransforming() || inks.length() == 0 )
    {
        return false;
    } else
    {
        d->pairs = inks;
        d->targetDirectory = targetDirectory;
        d->currentIndex = 0;
        d->transformedFiles.clear();
        return startTransformingCurrent();
    }
}

bool InkTransformer::isTransforming() const
{
    return d->currentIndex >= 0;
}

const QHash< InkPair, QString > &InkTransformer::transformedFiles() const
{
    return d->transformedFiles;
}

bool InkTransformer::startTransformingCurrent()
{
    InkPair input = d->pairs[ d->currentIndex ];
    QString output = FileUtils::findNonExistingFileName( d->targetDirectory, "transformed-ink", ".xml" );
    d->transformedFiles.insert( input, output );
    QStringList args;
    args << "--input" << input.first << "--matching-results" << input.second << "--output" << output;
    qDebug() << "Start transforming Ink XML file...";
    qDebug() << "Arguments: " << args;
    return d->runner->run( ToolsConfiguration::instance()->inkTransformer(), args );
}

void InkTransformer::finishedTransformingPair( bool, bool )
{
    d->currentIndex++;
    if ( d->currentIndex < d->pairs.length() )
    {
        startTransformingCurrent();
    } else
    {
        d->currentIndex = -1;
        d->pairs.clear();
        d->targetDirectory = QString();
        emit finishedTransforming();
    }
}

