/*
    formextract - Extract Form Data using Digital Ink and Scanned Forms
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "imagefilter.h"

#include "fileutils.h"
#include "programrunner.h"
#include "toolsconfiguration.h"
#include "formsconfiguration.h"

#include <QDebug>

class ImageFilter::ImageFilterPrivate
{

public:

    QStringList                 scannedImages;
    QString                     filterScannedTargetDirectory;
    QHash< QString, QString >   filteredScannedImages;
    int                         filterScannedCurrentIndex;
    ProgramRunner              *filterScannedRunner;

    ImageFilterPrivate( ImageFilter *imf )
        : scannedImages( QStringList() ),
          filterScannedTargetDirectory( QString() ),
          filteredScannedImages( QHash< QString, QString >() ),
          filterScannedCurrentIndex( -1 ),
          filterScannedRunner( new ProgramRunner( imf ) ),
          imageFilter( imf )
    {
    }

    virtual ~ImageFilterPrivate()
    {
    }


private:

    ImageFilter *imageFilter;

};

ImageFilter::ImageFilter( QObject *parent )
    : QObject( parent ),
      d( new ImageFilterPrivate( this ) )
{
    connect( d->filterScannedRunner, SIGNAL(finished(bool,bool)), this, SLOT(filterScannedImageFinished(bool,bool)) );
}

ImageFilter::~ImageFilter()
{
    delete d;
}

bool ImageFilter::filterScannedImages( const QStringList &scannedImages, const QString &targetDirectory )
{
    if ( isFilteringScannedImages() || scannedImages.length() == 0 )
    {
        return false;
    } else
    {
        d->scannedImages = scannedImages;
        d->filterScannedCurrentIndex = 0;
        d->filterScannedTargetDirectory = targetDirectory;
        d->filteredScannedImages.clear();

        return startFilterCurrentScanned();
    }
}

bool ImageFilter::isFilteringScannedImages() const
{
    return d->filterScannedCurrentIndex >= 0;
}

const QHash< QString, QString > &ImageFilter::filteredImages() const
{
    return d->filteredScannedImages;
}

bool ImageFilter::startFilterCurrentScanned()
{

    QString input = d->scannedImages[ d->filterScannedCurrentIndex ];
    QString output = FileUtils::findNonExistingFileName( d->filterScannedTargetDirectory,
                                                         FileUtils::basename( input ),
                                                         ".png" );
    d->filteredScannedImages.insert( input, output );

    QStringList args;
    args << "load"
            << "--filename" <<  input
            << "--name" << "input"
         << "resize"
            << "--input" << "input"
            << "--maxwidth" << "1500"
            << "--scalingmode" << "smooth"
            << "--aspectratiomode" << "keep"
            << "--output" << "input"
         << "colormask"
            << "--channel" << "0xff0000"
            << "--threshold" << "40"
            << "--input" << "input"
            << "--output" << "mask"
         << "mean"
            << "--in" << "mask"
            << "--radius" << "5"
            << "--out" << "mask"
         << "uniformblack"
            << "--in" << "mask"
            << "--threshold" << "30"
            << "--out" << "mask"
         << "removemask"
            << "--input" << "mask"
            << "--side" << "top"
            << "--output" << "mask"
         << "extractform"
            << "--input" << "input"
            << "--mask" << "mask"
            << "--output" << "output"
         << "uniformblack"
            << "--in" << "output"
            << "--threshold" << "150"
            << "--maxdist" << "30"
            << "--out" << "output"
         << "mean"
            << "--in" << "output"
            << "--radius" << "3"
            << "--out" << "output"
         << "uniformblack"
            << "--in" << "output"
            << "--threshold" << "200"
            << "--out" << "output"
         << "resize"
            << "--width" << QString::number( FormsConfiguration::instance()->formRect().width() )
            << "--height" << QString::number( FormsConfiguration::instance()->formRect().height() )
            << "--scalingmode" << "smooth"
            << "--aspectratiomode" << "discard"
            << "--input" << "output"
            << "--output" << "output"
         << "addborder" << "--input" << "output" << "--output" << "output" << "--color" << "white"
            << "--size-top" << QString::number( FormsConfiguration::instance()->formRect().top() )
            << "--size-left" << QString::number( FormsConfiguration::instance()->formRect().left() )
            << "--size-bottom" << QString::number( FormsConfiguration::instance()->formRect().top() )
            << "--size-right" << QString::number( FormsConfiguration::instance()->formRect().left() )
         << "save"
            << "--name" << "output"
            << "--filename" << output
         << "save"
            << "--name" << "mask"
            << "--filename" << output + "-mask.png"
         ;

    qDebug() << "Start filtering image...";
    qDebug() << "Arguments: " << args;
    return d->filterScannedRunner->run( ToolsConfiguration::instance()->imageFilter(), args );
}

void ImageFilter::filterScannedImageFinished( bool, bool )
{
    d->filterScannedCurrentIndex++;
    if ( d->filterScannedCurrentIndex < d->scannedImages.length() )
    {
        startFilterCurrentScanned();
    }  else
    {
        d->filterScannedCurrentIndex = -1;
        d->filterScannedTargetDirectory = QString();
        d->scannedImages.clear();
        emit filteredScannedImages();
    }
}


